package COPBonus.structure;

/*Generated by MPS */

import jetbrains.mps.lang.smodel.LanguageConceptIndex;
import jetbrains.mps.lang.smodel.LanguageConceptIndexBuilder;
import jetbrains.mps.smodel.adapter.ids.SConceptId;
import org.jetbrains.mps.openapi.language.SAbstractConcept;

public final class LanguageConceptSwitch {
  private final LanguageConceptIndex myIndex;
  public static final int BoolExpression = 0;
  public static final int BoolReturn = 1;
  public static final int BooleanDeclaration = 2;
  public static final int Class = 3;
  public static final int ElseIfStatement = 4;
  public static final int ElseStatement = 5;
  public static final int ForStatement = 6;
  public static final int Function = 7;
  public static final int IClassContent = 8;
  public static final int IControlStructure = 9;
  public static final int IExpression = 10;
  public static final int IReturnType = 11;
  public static final int IStatement = 12;
  public static final int IVariableDeclaration = 13;
  public static final int IfStatement = 14;
  public static final int IntExpression = 15;
  public static final int IntReturn = 16;
  public static final int IntegerDeclaration = 17;
  public static final int VarAssignment = 18;
  public static final int WhileStatement = 19;
  public static final int Worksheet = 20;

  public LanguageConceptSwitch() {
    LanguageConceptIndexBuilder builder = new LanguageConceptIndexBuilder(0xdaf995be71844e13L, 0x9b0d2b5a0b85f29bL);
    builder.put(0x9e709cab590cac8L, BoolExpression);
    builder.put(0x9e709cab5902179L, BoolReturn);
    builder.put(0x9e709cab5902167L, BooleanDeclaration);
    builder.put(0x9e709cab5902153L, Class);
    builder.put(0x9e709cab590ccd5L, ElseIfStatement);
    builder.put(0x9e709cab590cc73L, ElseStatement);
    builder.put(0x9e709cab590cd5dL, ForStatement);
    builder.put(0x9e709cab590215dL, Function);
    builder.put(0x9e709cab5902154L, IClassContent);
    builder.put(0x9e709cab590ca93L, IControlStructure);
    builder.put(0x9e709cab590ca3aL, IExpression);
    builder.put(0x9e709cab5902176L, IReturnType);
    builder.put(0x9e709cab5902170L, IStatement);
    builder.put(0x9e709cab5902160L, IVariableDeclaration);
    builder.put(0x9e709cab590cabaL, IfStatement);
    builder.put(0x9e709cab590cac5L, IntExpression);
    builder.put(0x9e709cab590217aL, IntReturn);
    builder.put(0x9e709cab5902166L, IntegerDeclaration);
    builder.put(0x9e709cab590ca64L, VarAssignment);
    builder.put(0x9e709cab590ce4eL, WhileStatement);
    builder.put(0x9e709cab58e300dL, Worksheet);
    myIndex = builder.seal();
  }

  /*package*/ int index(SConceptId cid) {
    return myIndex.index(cid);
  }

  public int index(SAbstractConcept concept) {
    return myIndex.index(concept);
  }
}
