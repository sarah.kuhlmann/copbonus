package COPBonus.structure;

/*Generated by MPS */

import jetbrains.mps.smodel.runtime.ConceptPresentationAspectBase;
import jetbrains.mps.smodel.runtime.ConceptPresentation;
import org.jetbrains.annotations.Nullable;
import org.jetbrains.mps.openapi.language.SAbstractConcept;
import jetbrains.mps.smodel.runtime.ConceptPresentationBuilder;

public class ConceptPresentationAspectImpl extends ConceptPresentationAspectBase {
  private ConceptPresentation props_BoolExpression;
  private ConceptPresentation props_BoolReturn;
  private ConceptPresentation props_BooleanDeclaration;
  private ConceptPresentation props_Class;
  private ConceptPresentation props_ElseIfStatement;
  private ConceptPresentation props_ElseStatement;
  private ConceptPresentation props_ForStatement;
  private ConceptPresentation props_Function;
  private ConceptPresentation props_IClassContent;
  private ConceptPresentation props_IControlStructure;
  private ConceptPresentation props_IExpression;
  private ConceptPresentation props_IReturnType;
  private ConceptPresentation props_IStatement;
  private ConceptPresentation props_IVariableDeclaration;
  private ConceptPresentation props_IfStatement;
  private ConceptPresentation props_IntExpression;
  private ConceptPresentation props_IntReturn;
  private ConceptPresentation props_IntegerDeclaration;
  private ConceptPresentation props_VarAssignment;
  private ConceptPresentation props_WhileStatement;
  private ConceptPresentation props_Worksheet;

  @Override
  @Nullable
  public ConceptPresentation getDescriptor(SAbstractConcept c) {
    StructureAspectDescriptor structureDescriptor = (StructureAspectDescriptor) myLanguageRuntime.getAspect(jetbrains.mps.smodel.runtime.StructureAspectDescriptor.class);
    switch (structureDescriptor.internalIndex(c)) {
      case LanguageConceptSwitch.BoolExpression:
        if (props_BoolExpression == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_BoolExpression = cpb.create();
        }
        return props_BoolExpression;
      case LanguageConceptSwitch.BoolReturn:
        if (props_BoolReturn == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("bool");
          props_BoolReturn = cpb.create();
        }
        return props_BoolReturn;
      case LanguageConceptSwitch.BooleanDeclaration:
        if (props_BooleanDeclaration == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_BooleanDeclaration = cpb.create();
        }
        return props_BooleanDeclaration;
      case LanguageConceptSwitch.Class:
        if (props_Class == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_Class = cpb.create();
        }
        return props_Class;
      case LanguageConceptSwitch.ElseIfStatement:
        if (props_ElseIfStatement == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("ElseIfStatement");
          props_ElseIfStatement = cpb.create();
        }
        return props_ElseIfStatement;
      case LanguageConceptSwitch.ElseStatement:
        if (props_ElseStatement == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("else");
          props_ElseStatement = cpb.create();
        }
        return props_ElseStatement;
      case LanguageConceptSwitch.ForStatement:
        if (props_ForStatement == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_ForStatement = cpb.create();
        }
        return props_ForStatement;
      case LanguageConceptSwitch.Function:
        if (props_Function == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_Function = cpb.create();
        }
        return props_Function;
      case LanguageConceptSwitch.IClassContent:
        if (props_IClassContent == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_IClassContent = cpb.create();
        }
        return props_IClassContent;
      case LanguageConceptSwitch.IControlStructure:
        if (props_IControlStructure == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_IControlStructure = cpb.create();
        }
        return props_IControlStructure;
      case LanguageConceptSwitch.IExpression:
        if (props_IExpression == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_IExpression = cpb.create();
        }
        return props_IExpression;
      case LanguageConceptSwitch.IReturnType:
        if (props_IReturnType == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_IReturnType = cpb.create();
        }
        return props_IReturnType;
      case LanguageConceptSwitch.IStatement:
        if (props_IStatement == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_IStatement = cpb.create();
        }
        return props_IStatement;
      case LanguageConceptSwitch.IVariableDeclaration:
        if (props_IVariableDeclaration == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          props_IVariableDeclaration = cpb.create();
        }
        return props_IVariableDeclaration;
      case LanguageConceptSwitch.IfStatement:
        if (props_IfStatement == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_IfStatement = cpb.create();
        }
        return props_IfStatement;
      case LanguageConceptSwitch.IntExpression:
        if (props_IntExpression == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_IntExpression = cpb.create();
        }
        return props_IntExpression;
      case LanguageConceptSwitch.IntReturn:
        if (props_IntReturn == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("IntReturn");
          props_IntReturn = cpb.create();
        }
        return props_IntReturn;
      case LanguageConceptSwitch.IntegerDeclaration:
        if (props_IntegerDeclaration == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_IntegerDeclaration = cpb.create();
        }
        return props_IntegerDeclaration;
      case LanguageConceptSwitch.VarAssignment:
        if (props_VarAssignment == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.rawPresentation("VarAssignment");
          props_VarAssignment = cpb.create();
        }
        return props_VarAssignment;
      case LanguageConceptSwitch.WhileStatement:
        if (props_WhileStatement == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_WhileStatement = cpb.create();
        }
        return props_WhileStatement;
      case LanguageConceptSwitch.Worksheet:
        if (props_Worksheet == null) {
          ConceptPresentationBuilder cpb = new ConceptPresentationBuilder();
          cpb.presentationByName();
          props_Worksheet = cpb.create();
        }
        return props_Worksheet;
    }
    return null;
  }
}
