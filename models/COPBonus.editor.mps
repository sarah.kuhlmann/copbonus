<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:ce647ab9-8fdd-4bc8-a78b-d9cd97c1ed13(COPBonus.editor)">
  <persistence version="9" />
  <languages>
    <use id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor" version="14" />
    <use id="aee9cad2-acd4-4608-aef2-0004f6a1cdbd" name="jetbrains.mps.lang.actions" version="4" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="5do6" ref="r:cd04f5b4-869e-41c6-be53-58dc09a0dcd8(COPBonus.structure)" implicit="true" />
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="18bc6592-03a6-4e29-a83a-7ff23bde13ba" name="jetbrains.mps.lang.editor">
      <concept id="1071666914219" name="jetbrains.mps.lang.editor.structure.ConceptEditorDeclaration" flags="ig" index="24kQdi" />
      <concept id="1140524381322" name="jetbrains.mps.lang.editor.structure.CellModel_ListWithRole" flags="ng" index="2czfm3">
        <property id="1140524450557" name="separatorText" index="2czwfO" />
        <child id="1140524464360" name="cellLayout" index="2czzBx" />
      </concept>
      <concept id="1106270549637" name="jetbrains.mps.lang.editor.structure.CellLayout_Horizontal" flags="nn" index="2iRfu4" />
      <concept id="1106270571710" name="jetbrains.mps.lang.editor.structure.CellLayout_Vertical" flags="nn" index="2iRkQZ" />
      <concept id="1237303669825" name="jetbrains.mps.lang.editor.structure.CellLayout_Indent" flags="nn" index="l2Vlx" />
      <concept id="1080736578640" name="jetbrains.mps.lang.editor.structure.BaseEditorComponent" flags="ig" index="2wURMF">
        <child id="1080736633877" name="cellModel" index="2wV5jI" />
      </concept>
      <concept id="1186414928363" name="jetbrains.mps.lang.editor.structure.SelectableStyleSheetItem" flags="ln" index="VPM3Z" />
      <concept id="1139848536355" name="jetbrains.mps.lang.editor.structure.CellModel_WithRole" flags="ng" index="1$h60E">
        <reference id="1140103550593" name="relationDeclaration" index="1NtTu8" />
      </concept>
      <concept id="1073389446423" name="jetbrains.mps.lang.editor.structure.CellModel_Collection" flags="sn" stub="3013115976261988961" index="3EZMnI">
        <child id="1106270802874" name="cellLayout" index="2iSdaV" />
        <child id="1073389446424" name="childCellModel" index="3EZMnx" />
      </concept>
      <concept id="1073389577006" name="jetbrains.mps.lang.editor.structure.CellModel_Constant" flags="sn" stub="3610246225209162225" index="3F0ifn">
        <property id="1073389577007" name="text" index="3F0ifm" />
      </concept>
      <concept id="1073389658414" name="jetbrains.mps.lang.editor.structure.CellModel_Property" flags="sg" stub="730538219796134133" index="3F0A7n" />
      <concept id="1219418625346" name="jetbrains.mps.lang.editor.structure.IStyleContainer" flags="ng" index="3F0Thp">
        <child id="1219418656006" name="styleItem" index="3F10Kt" />
      </concept>
      <concept id="1073389882823" name="jetbrains.mps.lang.editor.structure.CellModel_RefNode" flags="sg" stub="730538219795960754" index="3F1sOY" />
      <concept id="1073390211982" name="jetbrains.mps.lang.editor.structure.CellModel_RefNodeList" flags="sg" stub="2794558372793454595" index="3F2HdR" />
      <concept id="1166049232041" name="jetbrains.mps.lang.editor.structure.AbstractComponent" flags="ng" index="1XWOmA">
        <reference id="1166049300910" name="conceptDeclaration" index="1XX52x" />
      </concept>
    </language>
  </registry>
  <node concept="24kQdi" id="BB2sEPzQim">
    <ref role="1XX52x" to="5do6:BB2sEPzz0d" resolve="Worksheet" />
    <node concept="3EZMnI" id="BB2sEPzQio" role="2wV5jI">
      <node concept="3EZMnI" id="BB2sEPzQiv" role="3EZMnx">
        <node concept="VPM3Z" id="BB2sEPzQix" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEPzQiG" role="3EZMnx">
          <property role="3F0ifm" value="worksheet" />
        </node>
        <node concept="3F0A7n" id="BB2sEPzQiM" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="2iRfu4" id="BB2sEPzQi$" role="2iSdaV" />
        <node concept="3F0ifn" id="BB2sEPzYNk" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
      </node>
      <node concept="3F2HdR" id="BB2sEPzQj5" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEPzQi9" resolve="class" />
        <node concept="2iRkQZ" id="BB2sEPzQj7" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEPzYN_" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="BB2sEPzQir" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$26o">
    <ref role="1XX52x" to="5do6:BB2sEP$25j" resolve="Class" />
    <node concept="3EZMnI" id="BB2sEP$26U" role="2wV5jI">
      <node concept="2iRkQZ" id="BB2sEP$26X" role="2iSdaV" />
      <node concept="3EZMnI" id="BB2sEP$27b" role="3EZMnx">
        <node concept="2iRfu4" id="BB2sEP$27c" role="2iSdaV" />
        <node concept="VPM3Z" id="BB2sEP$27d" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$27h" role="3EZMnx">
          <property role="3F0ifm" value="class" />
        </node>
        <node concept="3F0A7n" id="BB2sEP$27s" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$27$" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
      </node>
      <node concept="3F2HdR" id="BB2sEP$28_" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$25n" resolve="classContent" />
        <node concept="2iRkQZ" id="BB2sEP$28B" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$27L" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$29d">
    <ref role="1XX52x" to="5do6:BB2sEP$25t" resolve="Function" />
    <node concept="3EZMnI" id="BB2sEP$29y" role="2wV5jI">
      <node concept="2iRkQZ" id="BB2sEP$29_" role="2iSdaV" />
      <node concept="3EZMnI" id="BB2sEP$29N" role="3EZMnx">
        <node concept="2iRfu4" id="BB2sEP$29O" role="2iSdaV" />
        <node concept="VPM3Z" id="BB2sEP$29P" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$29V" role="3EZMnx">
          <property role="3F0ifm" value="fun" />
        </node>
        <node concept="3F0A7n" id="BB2sEP$2cv" role="3EZMnx">
          <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$2a5" role="3EZMnx">
          <property role="3F0ifm" value="(" />
        </node>
        <node concept="3F2HdR" id="BB2sEP$2ai" role="3EZMnx">
          <property role="2czwfO" value="," />
          <ref role="1NtTu8" to="5do6:BB2sEP$25I" resolve="vars" />
          <node concept="2iRfu4" id="BB2sEP$2ak" role="2czzBx" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$2av" role="3EZMnx">
          <property role="3F0ifm" value=")" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$2aH" role="3EZMnx">
          <property role="3F0ifm" value=":" />
        </node>
        <node concept="3F1sOY" id="BB2sEP$2aX" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$28P" resolve="returnVar" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$2bf" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
      </node>
      <node concept="3F2HdR" id="BB2sEP$2bA" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$25N" resolve="statements" />
        <node concept="2iRkQZ" id="BB2sEP$2bC" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$2c5" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$88K">
    <ref role="1XX52x" to="5do6:BB2sEP$25A" resolve="IntegerDeclaration" />
    <node concept="3EZMnI" id="BB2sEP$88M" role="2wV5jI">
      <node concept="3F0ifn" id="BB2sEP$88W" role="3EZMnx">
        <property role="3F0ifm" value="int" />
      </node>
      <node concept="3F0A7n" id="BB2sEP$892" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="BB2sEP$88P" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$89d">
    <ref role="1XX52x" to="5do6:BB2sEP$25U" resolve="IntReturn" />
    <node concept="3EZMnI" id="BB2sEP$89i" role="2wV5jI">
      <node concept="3F0ifn" id="BB2sEP$89p" role="3EZMnx">
        <property role="3F0ifm" value="int" />
      </node>
      <node concept="l2Vlx" id="BB2sEP$89l" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$89z">
    <ref role="1XX52x" to="5do6:BB2sEP$25T" resolve="BoolReturn" />
    <node concept="3EZMnI" id="BB2sEP$89_" role="2wV5jI">
      <node concept="3F0ifn" id="BB2sEP$89J" role="3EZMnx">
        <property role="3F0ifm" value="bool" />
      </node>
      <node concept="l2Vlx" id="BB2sEP$89C" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$89T">
    <ref role="1XX52x" to="5do6:BB2sEP$25B" resolve="BooleanDeclaration" />
    <node concept="3EZMnI" id="BB2sEP$89V" role="2wV5jI">
      <node concept="3F0ifn" id="BB2sEP$8a5" role="3EZMnx">
        <property role="3F0ifm" value="bool" />
      </node>
      <node concept="3F0A7n" id="BB2sEP$8ab" role="3EZMnx">
        <ref role="1NtTu8" to="tpck:h0TrG11" resolve="name" />
      </node>
      <node concept="l2Vlx" id="BB2sEP$89Y" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$cDR">
    <ref role="1XX52x" to="5do6:BB2sEP$cD$" resolve="VarAssignment" />
    <node concept="3EZMnI" id="BB2sEP$cDT" role="2wV5jI">
      <node concept="3F1sOY" id="BB2sEP$cE0" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cDE" resolve="varDec" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$cE6" role="3EZMnx">
        <property role="3F0ifm" value="=" />
      </node>
      <node concept="3F1sOY" id="BB2sEP$cEe" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cDG" resolve="expr" />
      </node>
      <node concept="l2Vlx" id="BB2sEP$cDW" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$cFw">
    <ref role="1XX52x" to="5do6:BB2sEP$cEU" resolve="IfStatement" />
    <node concept="3EZMnI" id="BB2sEP$cFZ" role="2wV5jI">
      <node concept="2iRkQZ" id="BB2sEP$cG2" role="2iSdaV" />
      <node concept="3EZMnI" id="BB2sEP$cGg" role="3EZMnx">
        <node concept="2iRfu4" id="BB2sEP$cGh" role="2iSdaV" />
        <node concept="VPM3Z" id="BB2sEP$cGi" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$cKq" role="3EZMnx">
          <property role="3F0ifm" value="if" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cKv" role="3EZMnx">
          <property role="3F0ifm" value="(" />
        </node>
        <node concept="3F1sOY" id="BB2sEP$cKB" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$cFb" resolve="expression" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cKL" role="3EZMnx">
          <property role="3F0ifm" value=")" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cKX" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
      </node>
      <node concept="3F2HdR" id="BB2sEP$cL_" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cEZ" resolve="statements" />
        <node concept="2iRkQZ" id="BB2sEP$cLB" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$cLp" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="3F2HdR" id="BB2sEP$cVD" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cVn" resolve="elseif" />
        <node concept="2iRkQZ" id="BB2sEP$cVF" role="2czzBx" />
      </node>
      <node concept="3F1sOY" id="BB2sEP$cW8" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cVj" resolve="else" />
      </node>
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$cMf">
    <ref role="1XX52x" to="5do6:BB2sEP$cLN" resolve="ElseStatement" />
    <node concept="3EZMnI" id="BB2sEP$cMh" role="2wV5jI">
      <node concept="3EZMnI" id="BB2sEP$cMo" role="3EZMnx">
        <node concept="VPM3Z" id="BB2sEP$cMq" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$cMz" role="3EZMnx">
          <property role="3F0ifm" value="else" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cMH" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="BB2sEP$cMt" role="2iSdaV" />
      </node>
      <node concept="3F2HdR" id="BB2sEP$cMS" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cLO" resolve="statements" />
        <node concept="2iRkQZ" id="BB2sEP$cMU" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$cNb" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="BB2sEP$cMk" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$cNA">
    <ref role="1XX52x" to="5do6:BB2sEP$cNl" resolve="ElseIfStatement" />
    <node concept="3EZMnI" id="BB2sEP$cNC" role="2wV5jI">
      <node concept="3EZMnI" id="BB2sEP$cNJ" role="3EZMnx">
        <node concept="VPM3Z" id="BB2sEP$cNL" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$cNX" role="3EZMnx">
          <property role="3F0ifm" value="else if" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cO3" role="3EZMnx">
          <property role="3F0ifm" value="(" />
        </node>
        <node concept="3F1sOY" id="BB2sEP$cOg" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$cNo" resolve="expression" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cOq" role="3EZMnx">
          <property role="3F0ifm" value=")" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cOA" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="BB2sEP$cNO" role="2iSdaV" />
      </node>
      <node concept="3F2HdR" id="BB2sEP$cOR" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cNs" resolve="statements" />
        <node concept="2iRkQZ" id="BB2sEP$cOT" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$cPg" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="BB2sEP$cNF" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$cPV">
    <ref role="1XX52x" to="5do6:BB2sEP$cPt" resolve="ForStatement" />
    <node concept="3EZMnI" id="BB2sEP$cPX" role="2wV5jI">
      <node concept="3EZMnI" id="BB2sEP$cQ4" role="3EZMnx">
        <node concept="VPM3Z" id="BB2sEP$cQ6" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$cQh" role="3EZMnx">
          <property role="3F0ifm" value="for" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cQv" role="3EZMnx">
          <property role="3F0ifm" value="(" />
        </node>
        <node concept="3F1sOY" id="BB2sEP$cQB" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$cPu" resolve="init" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cQL" role="3EZMnx">
          <property role="3F0ifm" value="," />
        </node>
        <node concept="3F1sOY" id="BB2sEP$cQX" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$cPw" resolve="limit" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cRb" role="3EZMnx">
          <property role="3F0ifm" value="," />
        </node>
        <node concept="3F1sOY" id="BB2sEP$cRr" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$cPz" resolve="iteration" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cRH" role="3EZMnx">
          <property role="3F0ifm" value=")" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cS1" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="2iRfu4" id="BB2sEP$cQ9" role="2iSdaV" />
      </node>
      <node concept="3F2HdR" id="BB2sEP$cSq" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cPJ" resolve="statements" />
        <node concept="2iRkQZ" id="BB2sEP$cSs" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$cSX" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="BB2sEP$cQ0" role="2iSdaV" />
    </node>
  </node>
  <node concept="24kQdi" id="BB2sEP$cTr">
    <ref role="1XX52x" to="5do6:BB2sEP$cTe" resolve="WhileStatement" />
    <node concept="3EZMnI" id="BB2sEP$cTt" role="2wV5jI">
      <node concept="3EZMnI" id="BB2sEP$cT$" role="3EZMnx">
        <node concept="VPM3Z" id="BB2sEP$cTA" role="3F10Kt" />
        <node concept="3F0ifn" id="BB2sEP$cTM" role="3EZMnx">
          <property role="3F0ifm" value="while" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cTS" role="3EZMnx">
          <property role="3F0ifm" value="(" />
        </node>
        <node concept="3F1sOY" id="BB2sEP$cU0" role="3EZMnx">
          <ref role="1NtTu8" to="5do6:BB2sEP$cTf" resolve="expression" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cUa" role="3EZMnx">
          <property role="3F0ifm" value=")" />
        </node>
        <node concept="3F0ifn" id="BB2sEP$cUm" role="3EZMnx">
          <property role="3F0ifm" value="{" />
        </node>
        <node concept="l2Vlx" id="BB2sEP$cTD" role="2iSdaV" />
      </node>
      <node concept="3F2HdR" id="BB2sEP$cUB" role="3EZMnx">
        <ref role="1NtTu8" to="5do6:BB2sEP$cTh" resolve="statements" />
        <node concept="2iRkQZ" id="BB2sEP$cUD" role="2czzBx" />
      </node>
      <node concept="3F0ifn" id="BB2sEP$cV2" role="3EZMnx">
        <property role="3F0ifm" value="}" />
      </node>
      <node concept="2iRkQZ" id="BB2sEP$cTw" role="2iSdaV" />
    </node>
  </node>
</model>

