<?xml version="1.0" encoding="UTF-8"?>
<model ref="r:cd04f5b4-869e-41c6-be53-58dc09a0dcd8(COPBonus.structure)">
  <persistence version="9" />
  <languages>
    <use id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure" version="9" />
    <devkit ref="fbc25dd2-5da4-483a-8b19-70928e1b62d7(jetbrains.mps.devkit.general-purpose)" />
  </languages>
  <imports>
    <import index="tpck" ref="r:00000000-0000-4000-0000-011c89590288(jetbrains.mps.lang.core.structure)" implicit="true" />
  </imports>
  <registry>
    <language id="c72da2b9-7cce-4447-8389-f407dc1158b7" name="jetbrains.mps.lang.structure">
      <concept id="1169125787135" name="jetbrains.mps.lang.structure.structure.AbstractConceptDeclaration" flags="ig" index="PkWjJ">
        <property id="6714410169261853888" name="conceptId" index="EcuMT" />
        <property id="5092175715804935370" name="conceptAlias" index="34LRSv" />
        <child id="1071489727083" name="linkDeclaration" index="1TKVEi" />
      </concept>
      <concept id="1169125989551" name="jetbrains.mps.lang.structure.structure.InterfaceConceptDeclaration" flags="ig" index="PlHQZ">
        <child id="1169127546356" name="extends" index="PrDN$" />
      </concept>
      <concept id="1169127622168" name="jetbrains.mps.lang.structure.structure.InterfaceConceptReference" flags="ig" index="PrWs8">
        <reference id="1169127628841" name="intfc" index="PrY4T" />
      </concept>
      <concept id="1071489090640" name="jetbrains.mps.lang.structure.structure.ConceptDeclaration" flags="ig" index="1TIwiD">
        <property id="1096454100552" name="rootable" index="19KtqR" />
        <reference id="1071489389519" name="extends" index="1TJDcQ" />
        <child id="1169129564478" name="implements" index="PzmwI" />
      </concept>
      <concept id="1071489288298" name="jetbrains.mps.lang.structure.structure.LinkDeclaration" flags="ig" index="1TJgyj">
        <property id="1071599776563" name="role" index="20kJfa" />
        <property id="1071599893252" name="sourceCardinality" index="20lbJX" />
        <property id="1071599937831" name="metaClass" index="20lmBu" />
        <property id="241647608299431140" name="linkId" index="IQ2ns" />
        <reference id="1071599976176" name="target" index="20lvS9" />
      </concept>
    </language>
    <language id="ceab5195-25ea-4f22-9b92-103b95ca8c0c" name="jetbrains.mps.lang.core">
      <concept id="1169194658468" name="jetbrains.mps.lang.core.structure.INamedConcept" flags="ng" index="TrEIO">
        <property id="1169194664001" name="name" index="TrG5h" />
      </concept>
    </language>
  </registry>
  <node concept="1TIwiD" id="BB2sEPzz0d">
    <property role="EcuMT" value="713549832195551245" />
    <property role="TrG5h" value="Worksheet" />
    <property role="19KtqR" value="true" />
    <property role="34LRSv" value="worksheet" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEPzQi4" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="BB2sEPzQi6" role="PzmwI">
      <ref role="PrY4T" to="tpck:3fifI_xCcJN" resolve="ScopeProvider" />
    </node>
    <node concept="1TJgyj" id="BB2sEPzQi9" role="1TKVEi">
      <property role="IQ2ns" value="713549832195630217" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="class" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$25j" resolve="Class" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$25j">
    <property role="EcuMT" value="713549832195678547" />
    <property role="TrG5h" value="Class" />
    <property role="34LRSv" value="class" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="BB2sEP$25n" role="1TKVEi">
      <property role="IQ2ns" value="713549832195678551" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="classContent" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$25k" resolve="IClassContent" />
    </node>
    <node concept="PrWs8" id="BB2sEP$27q" role="PzmwI">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="PlHQZ" id="BB2sEP$25k">
    <property role="EcuMT" value="713549832195678548" />
    <property role="TrG5h" value="IClassContent" />
    <node concept="PrWs8" id="BB2sEP$25l" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$25t">
    <property role="EcuMT" value="713549832195678557" />
    <property role="TrG5h" value="Function" />
    <property role="34LRSv" value="fun" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$25u" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$25k" resolve="IClassContent" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$25I" role="1TKVEi">
      <property role="IQ2ns" value="713549832195678574" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="vars" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$25w" resolve="IVariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$28P" role="1TKVEi">
      <property role="IQ2ns" value="713549832195678773" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="returnVar" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$25Q" resolve="IReturnType" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$25N" role="1TKVEi">
      <property role="IQ2ns" value="713549832195678579" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="BB2sEP$25K" resolve="IStatement" />
    </node>
  </node>
  <node concept="PlHQZ" id="BB2sEP$25w">
    <property role="EcuMT" value="713549832195678560" />
    <property role="TrG5h" value="IVariableDeclaration" />
    <node concept="PrWs8" id="BB2sEP$25x" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="PrWs8" id="BB2sEP$25z" role="PrDN$">
      <ref role="PrY4T" node="BB2sEP$25k" resolve="IClassContent" />
    </node>
    <node concept="PrWs8" id="BB2sEP$pHZ" role="PrDN$">
      <ref role="PrY4T" node="BB2sEP$25K" resolve="IStatement" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$25A">
    <property role="EcuMT" value="713549832195678566" />
    <property role="TrG5h" value="IntegerDeclaration" />
    <property role="34LRSv" value="int" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$25E" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$25w" resolve="IVariableDeclaration" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$25B">
    <property role="EcuMT" value="713549832195678567" />
    <property role="TrG5h" value="BooleanDeclaration" />
    <property role="34LRSv" value="bool" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$25C" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$25w" resolve="IVariableDeclaration" />
    </node>
  </node>
  <node concept="PlHQZ" id="BB2sEP$25K">
    <property role="EcuMT" value="713549832195678576" />
    <property role="TrG5h" value="IStatement" />
    <node concept="PrWs8" id="BB2sEP$25L" role="PrDN$">
      <ref role="PrY4T" to="tpck:h0TrEE$" resolve="INamedConcept" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cEk" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721876" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expressions" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$cCU" resolve="IExpression" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cEm" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721878" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="controls" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$cEj" resolve="IControlStructure" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cEp" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721881" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="varDecs" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$25w" resolve="IVariableDeclaration" />
    </node>
  </node>
  <node concept="PlHQZ" id="BB2sEP$25Q">
    <property role="EcuMT" value="713549832195678582" />
    <property role="TrG5h" value="IReturnType" />
  </node>
  <node concept="1TIwiD" id="BB2sEP$25T">
    <property role="EcuMT" value="713549832195678585" />
    <property role="TrG5h" value="BoolReturn" />
    <property role="34LRSv" value="bool" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$25X" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$25Q" resolve="IReturnType" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$25U">
    <property role="EcuMT" value="713549832195678586" />
    <property role="TrG5h" value="IntReturn" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$25V" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$25Q" resolve="IReturnType" />
    </node>
  </node>
  <node concept="PlHQZ" id="BB2sEP$cCU">
    <property role="EcuMT" value="713549832195721786" />
    <property role="TrG5h" value="IExpression" />
    <node concept="PrWs8" id="BB2sEP$cCV" role="PrDN$">
      <ref role="PrY4T" node="BB2sEP$25K" resolve="IStatement" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cD$">
    <property role="EcuMT" value="713549832195721828" />
    <property role="TrG5h" value="VarAssignment" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="BB2sEP$cDE" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721834" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="varDec" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$25w" resolve="IVariableDeclaration" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cDG" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721836" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expr" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$cCU" resolve="IExpression" />
    </node>
  </node>
  <node concept="PlHQZ" id="BB2sEP$cEj">
    <property role="EcuMT" value="713549832195721875" />
    <property role="TrG5h" value="IControlStructure" />
    <node concept="PrWs8" id="BB2sEP$pHX" role="PrDN$">
      <ref role="PrY4T" node="BB2sEP$25K" resolve="IStatement" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cEU">
    <property role="EcuMT" value="713549832195721914" />
    <property role="TrG5h" value="IfStatement" />
    <property role="34LRSv" value="if" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$cEV" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$cEj" resolve="IControlStructure" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cEZ" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721919" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$25K" resolve="IStatement" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cFb" role="1TKVEi">
      <property role="IQ2ns" value="713549832195721931" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$cF8" resolve="BoolExpression" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cVn" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722967" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="elseif" />
      <property role="20lbJX" value="fLJekj5/_0__n" />
      <ref role="20lvS9" node="BB2sEP$cNl" resolve="ElseIfStatement" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cVj" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722963" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="else" />
      <ref role="20lvS9" node="BB2sEP$cLN" resolve="ElseStatement" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cF5">
    <property role="EcuMT" value="713549832195721925" />
    <property role="TrG5h" value="IntExpression" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$cF6" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$cCU" resolve="IExpression" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cF8">
    <property role="EcuMT" value="713549832195721928" />
    <property role="TrG5h" value="BoolExpression" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="PrWs8" id="BB2sEP$cF9" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$cCU" resolve="IExpression" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cLN">
    <property role="EcuMT" value="713549832195722355" />
    <property role="TrG5h" value="ElseStatement" />
    <property role="34LRSv" value="else" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="BB2sEP$cLO" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722356" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="BB2sEP$25K" resolve="IStatement" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cNl">
    <property role="EcuMT" value="713549832195722453" />
    <property role="TrG5h" value="ElseIfStatement" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="BB2sEP$cNo" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722456" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$cF8" resolve="BoolExpression" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cNs" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722460" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="BB2sEP$25K" resolve="IStatement" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cPt">
    <property role="EcuMT" value="713549832195722589" />
    <property role="TrG5h" value="ForStatement" />
    <property role="34LRSv" value="for" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="BB2sEP$cPu" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722590" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="init" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$25A" resolve="IntegerDeclaration" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cPw" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722592" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="limit" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$cF8" resolve="BoolExpression" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cPz" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722595" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="iteration" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$cF5" resolve="IntExpression" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cPJ" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722607" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="BB2sEP$25K" resolve="IStatement" />
    </node>
    <node concept="PrWs8" id="BB2sEP$cVh" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$cEj" resolve="IControlStructure" />
    </node>
  </node>
  <node concept="1TIwiD" id="BB2sEP$cTe">
    <property role="EcuMT" value="713549832195722830" />
    <property role="TrG5h" value="WhileStatement" />
    <property role="34LRSv" value="while" />
    <ref role="1TJDcQ" to="tpck:gw2VY9q" />
    <node concept="1TJgyj" id="BB2sEP$cTf" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722831" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="expression" />
      <property role="20lbJX" value="fLJekj4/_1" />
      <ref role="20lvS9" node="BB2sEP$cF8" resolve="BoolExpression" />
    </node>
    <node concept="1TJgyj" id="BB2sEP$cTh" role="1TKVEi">
      <property role="IQ2ns" value="713549832195722833" />
      <property role="20lmBu" value="fLJjDmT/aggregation" />
      <property role="20kJfa" value="statements" />
      <property role="20lbJX" value="fLJekj6/_1__n" />
      <ref role="20lvS9" node="BB2sEP$25K" resolve="IStatement" />
    </node>
    <node concept="PrWs8" id="BB2sEP$cVf" role="PzmwI">
      <ref role="PrY4T" node="BB2sEP$cEj" resolve="IControlStructure" />
    </node>
  </node>
</model>

